Bots EDI Translator
===================

.. image:: https://readthedocs.org/projects/bots/badge/?version=latest
    :alt: Documentation Status
    :scale: 100%
    :target: http://bots.readthedocs.io

.. image:: https://img.shields.io/pypi/l/bots.svg
   :target: https://raw.githubusercontent.com/bots-edi/bots/master/license.rst
   :alt: License

|
| Bots is complete software for EDI (Electronic Data Interchange),
| with a powerfull translator engine.
|

Features
--------

- Supports of many file formats: EDIFACT, x12, Tradacoms, XML, json, csv, ...
- Runs on Windows, Linux, OSX and Unix.
- Bots is very stable.
- Bots handles high volumes of EDI transactions.
- Bots is very flexible and can be configurated for your specific EDI needs.

|
| This is a fork of Bots from `Henk-Jan Ebbers`_.
|

Getting Started
---------------
|
| The documentation_ is a great place to get started.
|

License
-------
|
| Bots is licenced under GNU GENERAL PUBLIC LICENSE Version 3
| Full license text here: http://www.gnu.org/copyleft/gpl.html
|

Support
-------
|
| Commercial support by EDI Intelligentsia, https://www.edi-intelligentsia.com
|

.. _Henk-Jan Ebbers: http://bots.sourceforge.net/en/index.shtml
.. _documentation: https://bots.readthedocs.io
