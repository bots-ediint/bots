from __future__ import unicode_literals, print_function

from base64 import b64encode
from datetime import datetime
import glob
import os
import shutil
import socket
import subprocess
import sys
import time
import traceback

import django
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.core.mail import mail_managers
from django.db import connection, transaction
from django.db.models import Min
from django.shortcuts import get_object_or_404, Http404
from django.utils.translation import gettext as _
from django.views.decorators.csrf import requires_csrf_token

from . import (
    botsglobal,
    botslib,
    forms,
    job2queue,
    models,
    pluglib,
    py2html,
    viewlib,
)
from .botsconfig import (
    ERROR,
    EXTERNIN,
    EXTERNOUT,
    FILEIN,
    MERGED,
    NO_RETRY,
    OPEN,
    PROCESS,
    RESEND,
    SPLITUP,
    TRANSLATED,
)
from .bots_context import set_context

if sys.version_info[0] > 2:
    basestring = unicode = str


ERROR_500_TEMPLATE_NAME = '500.html'

IS_POPUP_VAR = 'popup'


@requires_csrf_token
def server_error(request, template_name=ERROR_500_TEMPLATE_NAME):
    """
    The 500 error handler.
    Templates: `500.html`
    Context: None
    """
    exc_info = traceback.format_exc(None)
    botsglobal.logger.error(_('Ran into server error: "%(error)s"'), {'error': exc_info})
    try:
        template = django.template.loader.get_template(template_name)
    except django.template.exceptions.TemplateDoesNotExist:
        if template_name != ERROR_500_TEMPLATE_NAME:
            # Reraise if it's a missing custom template.
            raise
        return django.http.HttpResponseServerError(
            '<h1>Server Error (500)</h1>', content_type='text/html')

    context = {}
    if botsglobal.ini.get('webserver', 'debug', False) == 'True':
        context['exc_info'] = exc_info
    context.update(set_context(request))
    return django.http.HttpResponseServerError(template.render(context))


def index(request):
    """
    when using eg http://localhost:8080
    index can be reached without being logged in.
    most of the time user is redirected to '/home'
    """
    return django.shortcuts.render(request, 'admin/base.html')


@login_required
def home(request):
    """Display bots environment infos"""
    return django.shortcuts.render(request, 'bots/about.html', {'botsinfo': botslib.botsinfo()})


@login_required
@permission_required('bots.view_report', login_url='/')
def reports(request):
    context = {'title': _('Reports')}
    if request.method == 'GET':
        if IS_POPUP_VAR in request.GET:
            context['is_popup'] = True
        cleaned_data = viewlib.load_selection(request, forms.SelectReports)
        if cleaned_data:
            cleaned_data.update({k: v for k, v in request.GET.items()})
            if cleaned_data.get(IS_POPUP_VAR):
                context['is_popup'] = True
            context['form'] = formin = forms.SelectReports(cleaned_data, request=request)
            if 'select' in request.GET or not formin.is_valid():
                return django.shortcuts.render(request, formin.template, context)
            cleaned_data = formin.cleaned_data

        elif 'select' in request.GET:
            # from menu:select->reports
            context['form'] = form = forms.SelectReports(request=request)
            if len(request.GET) > 1:
                form.initial.update(form.initials)
                form.initial.update({k: v for k, v in request.GET.items()})
                context['form'] = form = forms.SelectReports(form.initial, request=request)
                form.is_valid()
            # go to the SelectReports form
            return django.shortcuts.render(request, form.template, context)

        else:
            # from menu:run->report
            context['form'] = form = forms.ViewReports(request=request)
            cleaned_data = form.initials
            if len(request.GET) > 0:
                form.initial.update(form.initials)
                form.initial.update({k: v for k, v in request.GET.items()})
                context['form'] = formin = forms.ViewReports(form.initial, request=request)
                if not formin.is_valid():
                    return django.shortcuts.render(request, formin.template, context)
                if formin.cleaned_data.get('idta'):
                    cleaned_data.update({
                        'idta': formin.cleaned_data['idta'],
                        'datefrom': '', 'dateuntil': '',
                        'popup': formin.cleaned_data['popup'],
                    })
                else:
                    cleaned_data = formin.cleaned_data
        # go to default report-query

    else:
        # request.method == 'POST'
        # is_popup
        if request.POST.get(IS_POPUP_VAR):
            context['is_popup'] = True

        # selection from selectform
        if request.POST.get('selection_name') \
                or 'saveselection' in request.POST \
                or 'deleteselection' in request.POST:
            # from SelectReports form
            context['form'] = formin = forms.SelectReports(request.POST, request=request)
            if not formin.is_valid():
                return django.shortcuts.render(request, formin.template, context)
            if 'deleteselection' in request.POST:
                # delete selection
                viewlib.delete_selection(formin)
            else:
                # Save selection
                viewlib.save_selection(formin)
            if not 'fromselect' in request.POST:
                context['form'] = form = forms.SelectReports(formin.cleaned_data, request=request)
                return django.shortcuts.render(request, form.template, context)
        else:
            # from ViewReports form
            context['form'] = formin = forms.ViewReports(request.POST, request=request)

        if not formin.is_valid():
            return django.shortcuts.render(request, formin.template, context)

        if '2select' in request.POST:
            # from ViewReports form using button change selection
            context['form'] = form = forms.SelectReports(formin.cleaned_data, request=request)
            return django.shortcuts.render(request, form.template, context)
        if 'permlink' in request.POST:
            # Redirect to permanent url
            return django.shortcuts.redirect(viewlib.get_selection_url(formin))

        action, idtas = None, []
        if 'action' in request.POST and request.POST.get('actions'):
            action = request.POST.get('actions')
            idtas = [viewlib.safe_int(i) for i in request.POST.getlist('sel')]
        elif request.POST.get('delete'):
            action = 'delete'
            idtas = [viewlib.safe_int(request.POST['delete'])]

        if action == 'delete' and idtas:
            if request.user.has_perm('bots.delete_report'):
                for idta in idtas:
                    # delete report > filereports > tas
                    get_object_or_404(models.report, idta=idta).delete()
                txt = _('Reports deleted: %s') % idtas
                botsglobal.logger.info(txt)
                messages.add_message(request, messages.INFO, txt)
            else:
                warn = _('User %(user)s try to perform unauthorised request: %(request)s') % {
                    'user': request.user, 'request': request.POST}
                botsglobal.logger.warning(warn)

        # from ViewReports, next page etc
        viewlib.handlepagination(request.POST, formin)
        cleaned_data = formin.cleaned_data

    # normal report-query with parameters
    query = models.report.objects.all()
    context['queryset'] = viewlib.filterquery(query, cleaned_data, context=context)
    context['form'] = form = forms.ViewReports(initial=cleaned_data, request=request)
    return django.shortcuts.render(request, form.template, context)


@login_required
@permission_required('bots.view_filereport', login_url='/')
def incoming(request):
    context = {'title': _('Incoming')}
    if request.method == 'GET':
        if IS_POPUP_VAR in request.GET:
            context['is_popup'] = True
        cleaned_data = viewlib.load_selection(request, forms.SelectIncoming)
        if cleaned_data:
            cleaned_data.update({k: v for k, v in request.GET.items()})
            if cleaned_data.get(IS_POPUP_VAR):
                context['is_popup'] = True
            context['form'] = formin = forms.SelectIncoming(cleaned_data, request=request)
            if 'select' in request.GET or not formin.is_valid():
                return django.shortcuts.render(request, formin.template, context)
            cleaned_data = formin.cleaned_data

        elif 'select' in request.GET:
            # from menu:select->incoming
            context['form'] = form = forms.SelectIncoming(request=request)
            if len(request.GET) > 1:
                form.initial.update(form.initials)
                form.initial.update({k: v for k, v in request.GET.items()})
                context['form'] = form = forms.SelectIncoming(form.initial, request=request)
                form.is_valid()
            # go to the SelectIncoming form
            return django.shortcuts.render(request, form.template, context)

        else:
            # from menu:run->incoming
            context['form'] = form = forms.ViewIncoming(request=request)
            cleaned_data = form.initials
            if len(request.GET) > 0:
                form.initial.update(form.initials)
                form.initial.update({k: v for k, v in request.GET.items()})
                context['form'] = formin = forms.ViewIncoming(form.initial, request=request)
                if not formin.is_valid():
                    return django.shortcuts.render(request, formin.template, context)
                if formin.cleaned_data.get('idta') or formin.cleaned_data.get('reportidta'):
                    cleaned_data.update({
                        'idta': formin.cleaned_data['idta'],
                        'reportidta': formin.cleaned_data['reportidta'],
                        'datefrom': '', 'dateuntil': '',
                        'admlink': formin.cleaned_data['admlink'],
                        'popup': formin.cleaned_data['popup'],
                    })
                else:
                    cleaned_data = formin.cleaned_data
        # go to default incoming-query using these default parameters

    else:
        # request.method == 'POST'

        # is_popup
        if request.POST.get(IS_POPUP_VAR):
            context['is_popup'] = True

        # selection
        if request.POST.get('selection_name') \
                or 'saveselection' in request.POST \
                or 'deleteselection' in request.POST:
            # from SelectIncoming form
            context['form'] = formin = forms.SelectIncoming(request.POST, request=request)
            if not formin.is_valid():
                return django.shortcuts.render(request, formin.template, context)
            if 'deleteselection' in request.POST:
                # delete selection
                viewlib.delete_selection(formin)
            else:
                # Save selection
                viewlib.save_selection(formin)
            if not 'fromselect' in request.POST:
                context['form'] = form = forms.SelectIncoming(formin.cleaned_data, request=request)
                return django.shortcuts.render(request, form.template, context)

        else:
            # from ViewIncoming form, check this form first
            context['form'] = formin = forms.ViewIncoming(request.POST, request=request)

        if not formin.is_valid():
            return django.shortcuts.render(request, formin.template, context)

        if '2select' in request.POST:
            # from ViewIncoming form using button change selection
            context['form'] = form = forms.SelectIncoming(formin.cleaned_data, request=request)
            return django.shortcuts.render(request, form.template, context)
        if '2outgoing' in request.POST:
            # from ViewIncoming form, using button 'outgoing (same selection)'
            request.POST = viewlib.changepostparameters(request.POST, soort='in2out')
            return outgoing(request)
        if '2process' in request.POST:
            # from ViewIncoming form, using button 'process errors (same selection)'
            request.POST = viewlib.changepostparameters(request.POST, soort='2process')
            return process(request)
        if '2confirm' in request.POST:
            # from ViewIncoming form, using button 'confirm (same selection)'
            request.POST = viewlib.changepostparameters(request.POST, soort='in2confirm')
            return confirm(request)
        if '2report' in request.POST:
            # from ViewIncoming form, using button 'reports (same selection)'
            request.POST = viewlib.changepostparameters(request.POST, soort='in2report')
            return reports(request)
        if 'permlink' in request.POST:
            # get selection permanent url
            return django.shortcuts.redirect(viewlib.get_selection_url(formin))

        action, idtas = None, []
        if 'action' in request.POST and request.POST.get('actions'):
            action = request.POST.get('actions')
            idtas = [viewlib.safe_int(i) for i in request.POST.getlist('sel')]
        elif request.POST.get('delete'):
            action = 'delete'
            idtas = [viewlib.safe_int(request.POST['delete'])]
        elif 'rereceive' in request.POST:
            action = 'rereceive'
            idtas = [viewlib.safe_int(request.POST['rereceive'])]

        if action == 'delete' and idtas:
            if request.user.has_perm('bots.delete_filereport'):
                for idta in idtas:
                    # delete filereport
                    get_object_or_404(models.filereport, idta=idta).delete()
                txt = _('Filereports deleted: %s') % idtas
                botsglobal.logger.info(txt)
                messages.add_message(request, messages.INFO, txt)
            else:
                warn = _('User %(user)s try to perform unauthorised request: %(request)s') % {
                    'user': request.user, 'request': request.POST}
                botsglobal.logger.warning(warn)

        elif action == 'rereceive':
            if request.user.has_perm('bots.change_mutex'):
                for idta in idtas:
                    filereport = get_object_or_404(models.filereport, idta=idta)
                    if filereport.fromchannel:
                        # for resend files fromchannel has no value.
                        # (do not rereceive resend items)
                        filereport.retransmit = not filereport.retransmit
                        filereport.save()
            else:
                notification = _('No rights for this operation.')
                botsglobal.logger.info(notification)
                messages.add_message(request, messages.ERROR, notification)

        elif 'rereceiveall' in request.POST:
            if request.user.has_perm('bots.change_mutex'):
                # from ViewIncoming form using button 'rereceive all'
                # select all objects with parameters and set retransmit
                query = models.filereport.objects.all()
                incomingfiles = viewlib.filterquery(query, formin.cleaned_data, paginate=False)
                # for resend files fromchannel has no value.
                # (do not rereceive resend items)
                for incomingfile in incomingfiles:
                    if incomingfile.fromchannel:
                        incomingfile.retransmit = not incomingfile.retransmit
                        incomingfile.save()
            else:
                warn = _('User %(user)s try to perform unauthorised request: %(request)s') % {
                    'user': request.user, 'request': request.POST}
                botsglobal.logger.warning(warn)

        # from ViewIncoming, next page etc
        viewlib.handlepagination(request.POST, formin)
        cleaned_data = formin.cleaned_data

    # normal incoming-query with parameters
    query = models.filereport.objects.all()
    context['queryset'] = viewlib.filterquery(query, cleaned_data, incoming=True, context=context)
    context['form'] = form = forms.ViewIncoming(initial=cleaned_data, request=request)
    if cleaned_data.get('lastrun'):
        context['title'] += ' - lastrun'
    return django.shortcuts.render(request, form.template, context)


@login_required
@permission_required('bots.view_filereport', login_url='/')
def outgoing(request):
    context = {'title': _('Outgoing')}
    if request.method == 'GET':
        if IS_POPUP_VAR in request.GET:
            context['is_popup'] = True
        cleaned_data = viewlib.load_selection(request, forms.SelectOutgoing)
        if cleaned_data:
            cleaned_data.update({k: v for k, v in request.GET.items()})
            if cleaned_data.get(IS_POPUP_VAR):
                context['is_popup'] = True
            context['form'] = formin = forms.SelectOutgoing(cleaned_data, request=request)
            if 'select' in request.GET or not formin.is_valid():
                return django.shortcuts.render(request, formin.template, context)
            cleaned_data = formin.cleaned_data

        elif 'select' in request.GET:
            # from menu:select->outgoing
            context['form'] = form = forms.SelectOutgoing(request=request)
            if len(request.GET) > 1:
                form.initial.update(form.initials)
                form.initial.update({k: v for k, v in request.GET.items()})
                context['form'] = form = forms.SelectOutgoing(form.initial, request=request)
                form.is_valid()
            return django.shortcuts.render(request, form.template, context)

        else:
            # from menu:run->outgoing
            context['form'] = form = forms.ViewOutgoing(request=request)
            cleaned_data = form.initials
            if len(request.GET) > 0:
                form.initial.update(form.initials)
                form.initial.update({k: v for k, v in request.GET.items()})
                context['form'] = formin = forms.ViewOutgoing(form.initial, request=request)
                if not formin.is_valid():
                    return django.shortcuts.render(request, formin.template, context)
                if formin.cleaned_data.get('idta'):
                    cleaned_data.update({
                        'idta': formin.cleaned_data['idta'],
                        'datefrom': '', 'dateuntil': '',
                        'admlink': formin.cleaned_data['admlink'],
                        'popup': formin.cleaned_data['popup'],
                    })
                else:
                    cleaned_data = formin.cleaned_data
        # go to default outgoing-query using these default parameters

    else:
        # request.method == 'POST'

        # is_popup
        if request.POST.get(IS_POPUP_VAR):
            context['is_popup'] = True

        # selection
        if request.POST.get('selection_name') \
                or 'saveselection' in request.POST \
                or 'deleteselection' in request.POST:
            # from SelectOutgoing form
            context['form'] = formin = forms.SelectOutgoing(request.POST, request=request)
            if not formin.is_valid():
                return django.shortcuts.render(request, formin.template, context)
            if 'deleteselection' in request.POST:
                # delete selection
                viewlib.delete_selection(formin)
            else:
                # Save selection
                viewlib.save_selection(formin)
            if not 'fromselect' in request.POST:
                context['form'] = form = forms.SelectOutgoing(formin.cleaned_data, request=request)
                return django.shortcuts.render(request, form.template, context)
        else:
            # from ViewOutgoing form, check this form first
            context['form'] = formin = forms.ViewOutgoing(request.POST, request=request)

        if not formin.is_valid():
            return django.shortcuts.render(request, formin.template, context)

        if '2select' in request.POST:
            # from ViewOutgoing form using button change selection
            context['form'] = form = forms.SelectOutgoing(formin.cleaned_data, request=request)
            return django.shortcuts.render(request, form.template, context)
        if '2incoming' in request.POST:
            # from ViewOutgoing form, using button 'incoming (same selection)'
            request.POST = viewlib.changepostparameters(request.POST, soort='out2in')
            return incoming(request)
        if '2process' in request.POST:
            # from ViewOutgoing form, using button 'process errors (same selection)'
            request.POST = viewlib.changepostparameters(request.POST, soort='2process')
            return process(request)
        if '2confirm' in request.POST:
            # from ViewOutgoing form, using button 'confirm (same selection)'
            request.POST = viewlib.changepostparameters(request.POST, soort='out2confirm')
            return confirm(request)
        if 'permlink' in request.POST:
            # Redirect to permanent url
            return django.shortcuts.redirect(viewlib.get_selection_url(formin))

        if request.user.has_perm('bots.change_mutex'):
            action, idtas = None, []
            if 'action' in request.POST and request.POST.get('actions'):
                action = request.POST.get('actions')
                idtas = [viewlib.safe_int(i) for i in request.POST.getlist('sel')]
            elif 'resend' in request.POST:
                action = 'resend'
                idtas = [viewlib.safe_int(request.POST['resend'])]
            elif 'noautomaticretry' in request.POST:
                action = 'noautomaticretry'
                idtas = [viewlib.safe_int(request.POST['noautomaticretry'])]
            if action and idtas:
                if action == 'resend':
                    for idta in idtas:
                        ta_object = get_object_or_404(models.ta, idta=idta)
                        if ta_object.statust != RESEND:
                            # can only resend last file
                            ta_object.retransmit = not ta_object.retransmit
                            ta_object.save()
                elif action == 'noautomaticretry':
                    for idta in idtas:
                        ta_object = get_object_or_404(models.ta, idta=idta)
                        if ta_object.statust == ERROR:
                            ta_object.statust = NO_RETRY
                            ta_object.save()

            elif 'resendall' in request.POST:
                # from ViewOutgoing form using button 'resend all'
                # select all objects with parameters and set retransmit
                query = models.ta.objects.filter(status=EXTERNOUT)
                outgoingfiles = viewlib.filterquery(query, formin.cleaned_data, paginate=False)
                for outgoingfile in outgoingfiles:
                    # can only resend last file
                    if outgoingfile.statust != RESEND:
                        outgoingfile.retransmit = not outgoingfile.retransmit
                        outgoingfile.save()

        # from ViewIncoming, next page etc
        viewlib.handlepagination(request.POST, formin)
        cleaned_data = formin.cleaned_data

    # normal outgoing-query with parameters
    query = models.ta.objects.filter(status=EXTERNOUT)
    context['queryset'] = viewlib.filterquery(query, cleaned_data, context=context)
    context['form'] = form = forms.ViewOutgoing(initial=cleaned_data, request=request)
    if cleaned_data.get('lastrun'):
        context['title'] += ' - lastrun'
    return django.shortcuts.render(request, form.template, context)


@login_required
@permission_required('bots.view_ta', login_url='/')
def document(request):
    context = {'title': _('Documents')}
    if request.method == 'GET':
        if IS_POPUP_VAR in request.GET:
            context['is_popup'] = True
        cleaned_data = viewlib.load_selection(request, forms.SelectDocument)
        if cleaned_data:
            cleaned_data.update({k: v for k, v in request.GET.items()})
            if cleaned_data.get(IS_POPUP_VAR):
                context['is_popup'] = True
            context['form'] = formin = forms.SelectDocument(cleaned_data, request=request)
            if 'select' in request.GET or not formin.is_valid():
                return django.shortcuts.render(request, formin.template, context)
            cleaned_data = formin.cleaned_data

        elif 'select' in request.GET:
            # from menu:select->document
            context['form'] = form = forms.SelectDocument(request=request)
            if len(request.GET) > 1:
                form.initial.update(form.initials)
                form.initial.update({k: v for k, v in request.GET.items()})
                context['form'] = form = forms.SelectDocument(form.initial, request=request)
                form.is_valid()
            return django.shortcuts.render(request, form.template, context)

        else:
            context['form'] = form = forms.ViewDocument(request=request)
            cleaned_data = form.initials
            if len(request.GET) > 0:
                form.initial.update(form.initials)
                form.initial.update({k: v for k, v in request.GET.items()})
                context['form'] = formin = forms.ViewDocument(form.initial, request=request)
                if not formin.is_valid():
                    return django.shortcuts.render(request, formin.template, context)
                if formin.cleaned_data.get('idta'):
                    cleaned_data.update({
                        'idta': formin.cleaned_data['idta'],
                        'datefrom': '', 'dateuntil': '',
                        'admlink': formin.cleaned_data['admlink'],
                        'popup': formin.cleaned_data['popup'],
                    })
                else:
                    cleaned_data = formin.cleaned_data
        # go to default document-query using these default parameters

    else:
        # request.method == 'POST'

        # is_popup
        if request.POST.get(IS_POPUP_VAR):
            context['is_popup'] = True

        # selection
        if request.POST.get('selection_name') \
                or 'saveselection' in request.POST \
                or 'deleteselection' in request.POST:
            # from SelectDocument form
            formin = forms.SelectDocument(request.POST, request=request)
            if not formin.is_valid():
                context['form'] = formin
                return django.shortcuts.render(request, formin.template, context)
            if 'deleteselection' in request.POST:
                # delete selection
                viewlib.delete_selection(formin)
            else:
                # Save selection
                viewlib.save_selection(formin)
            if not 'fromselect' in request.POST:
                context['form'] = form = forms.SelectDocument(formin.cleaned_data, request=request)
                return django.shortcuts.render(request, form.template, context)

        else:
            # from ViewDocument form, check this form first
            context['form'] = formin = forms.ViewDocument(request.POST, request=request)

        if not formin.is_valid():
            return django.shortcuts.render(request, formin.template, context)

        if '2select' in request.POST:
            # coming from ViewDocument, change the selection criteria, go to select form
            context['form'] = form = forms.SelectDocument(formin.cleaned_data, request=request)
            return django.shortcuts.render(request, form.template, context)
        if 'permlink' in request.POST:
            # Redirect to permanent url
            return django.shortcuts.redirect(viewlib.get_selection_url(formin))

        if 'rereceive' in request.POST and request.user.has_perm('bots.change_mutex'):
            # coming from ViewDocument, no reportidta
            idta = request.POST['rereceive']
            rootta = viewlib.django_trace_origin(viewlib.safe_int(idta), {'status': EXTERNIN})
            if rootta:
                filereport = get_object_or_404(models.filereport, idta=rootta[0].idta)
                filereport.retransmit = not filereport.retransmit
                filereport.save()

        # coming from ViewDocument, next page etc
        viewlib.handlepagination(request.POST, formin)
        cleaned_data = formin.cleaned_data

    if cleaned_data.get('allstatus'):
        # All ta in query except PROCESS
        query = models.ta.objects.exclude(status=PROCESS)
    else:
        # normal document-query with parameters
        query = models.ta.objects.filter(
            django.db.models.Q(status=SPLITUP) | django.db.models.Q(status=TRANSLATED)
        )
    context['queryset'] = pquery = viewlib.filterquery(query, cleaned_data, context=context)
    viewlib.trace_document(pquery)
    context['form'] = form = forms.ViewDocument(initial=cleaned_data, request=request)
    if cleaned_data.get('status') == SPLITUP:
        context['title'] = _('Document-in')
    elif cleaned_data.get('status') == TRANSLATED:
        context['title'] = _('Document-out')
    if cleaned_data.get('lastrun'):
        context['title'] += ' - lastrun'
    return django.shortcuts.render(request, form.template, context)


@login_required
@permission_required('bots.view_filereport', login_url='/')
def process(request):
    context = {'title': _('Process')}
    if request.method == 'GET':
        if IS_POPUP_VAR in request.GET:
            context['is_popup'] = True
        cleaned_data = viewlib.load_selection(request, forms.SelectProcess)
        if cleaned_data:
            cleaned_data.update({k: v for k, v in request.GET.items()})
            if cleaned_data.get(IS_POPUP_VAR):
                context['is_popup'] = True
            context['form'] = formin = forms.SelectProcess(cleaned_data, request=request)
            if 'select' in request.GET or not formin.is_valid():
                return django.shortcuts.render(request, formin.template, context)
            cleaned_data = formin.cleaned_data

        elif 'select' in request.GET:
            # from menu:select->process
            context['form'] = form = forms.SelectProcess(request=request)
            if len(request.GET) > 1:
                form.initial.update(form.initials)
                form.initial.update({k: v for k, v in request.GET.items()})
                context['form'] = form = forms.SelectProcess(form.initial, request=request)
                form.is_valid()
            return django.shortcuts.render(request, form.template, context)

        else:
            # from menu: process->Errors
            context['form'] = form = forms.ViewProcess(request=request)
            cleaned_data = form.initials
            if len(request.GET) > 0:
                form.initial.update(form.initials)
                form.initial.update({k: v for k, v in request.GET.items()})
                context['form'] = formin = forms.ViewProcess(form.initial, request=request)
                if not formin.is_valid():
                    return django.shortcuts.render(request, formin.template, context)
                if formin.cleaned_data.get('idta'):
                    cleaned_data.update({
                        'idta': formin.cleaned_data['idta'],
                        'datefrom': '', 'dateuntil': '',
                        'admlink': formin.cleaned_data['admlink'],
                        'popup': formin.cleaned_data['popup'],
                    })
                else:
                    cleaned_data = formin.cleaned_data
        # go to default process-query using these default parameters

    else:
        # request.method == 'POST'

        # is_popup
        if request.POST.get(IS_POPUP_VAR):
            context['is_popup'] = True

        # selection
        if request.POST.get('selection_name') \
                or 'saveselection' in request.POST \
                or 'deleteselection' in request.POST:
            # from SelectProcess form
            context['form'] = formin = forms.SelectProcess(request.POST, request=request)
            if not formin.is_valid():
                return django.shortcuts.render(request, formin.template, context)
            if 'deleteselection' in request.POST:
                # delete selection
                viewlib.delete_selection(formin)
            else:
                # Save selection
                viewlib.save_selection(formin)
            if not 'fromselect' in request.POST:
                context['form'] = form = forms.SelectProcess(formin.cleaned_data, request=request)
                return django.shortcuts.render(request, form.template, context)
        else:
            # from ViewProcess form, check this form first
            context['form'] = formin = forms.ViewProcess(request.POST, request=request)

        if not formin.is_valid():
            return django.shortcuts.render(request, formin.template, context)

        if '2select' in request.POST:
            # coming from ViewProcess, change the selection criteria, go to select form
            context['form'] = form = forms.SelectProcess(formin.cleaned_data, request=request)
            return django.shortcuts.render(request, form.template, context)
        if '2incoming' in request.POST:
            # coming from ViewProcess, go to incoming form using same criteria
            request.POST = viewlib.changepostparameters(request.POST, soort='fromprocess')
            return incoming(request)
        if '2outgoing' in request.POST:
            # coming from ViewProcess, go to outgoing form using same criteria
            request.POST = viewlib.changepostparameters(request.POST, soort='fromprocess')
            return outgoing(request)
        if 'permlink' in request.POST:
            # Redirect to permanent url
            return django.shortcuts.redirect(viewlib.get_selection_url(formin))

        # coming from ViewProcess
        viewlib.handlepagination(request.POST, formin)
        cleaned_data = formin.cleaned_data

    # normal process-query with parameters
    query = models.ta.objects.filter(status=PROCESS, statust=ERROR)
    context['queryset'] = viewlib.filterquery(query, cleaned_data, context=context)
    context['form'] = form = forms.ViewProcess(initial=cleaned_data, request=request)
    return django.shortcuts.render(request, form.template, context)


@login_required
@permission_required('bots.view_ta', login_url='/')
def detail(request):
    """
    in: the idta, either as parameter in or out.
    in: is idta of incoming file.
    out: idta of outgoing file, need to trace back for incoming file.
    return list of ta's for display in detail template.
    This list is formatted and ordered for display.
    first, get a tree (trace) starting with the incoming ta ;
    than make up the details for the trace
    """
    if request.method == 'GET':
        rootta = None
        if 'inidta' in request.GET:
            # from incoming screen
            rootta = get_object_or_404(
                models.ta.objects, idta=viewlib.safe_int(request.GET['inidta'])
            )
        elif request.GET.get('outidta'):
            # from outgoing screen: trace back to EXTERNIN first
            rootta = viewlib.django_trace_origin(
                viewlib.safe_int(request.GET['outidta']), {'status': EXTERNIN}
            )
            if rootta:
                rootta = rootta[0]
        if not rootta:
            raise Http404()
        viewlib.gettrace(rootta)
        detaillist = viewlib.trace2detail(rootta)
        channels = set([ta.channel for ta in detaillist])
        channels_types = {
            channel.idchannel: channel.type
            for channel in models.channel.objects.filter(idchannel__in=channels)
        }
        context = {
            'detaillist': detaillist,
            'rootta': rootta,
            'title': _('Details') + ' %s' % rootta.idta,
            'partners': models.getactivepartners(),
            'inactive_partners': models.getinactivepartners(),
            'channels_types': channels_types,
        }
        if IS_POPUP_VAR in request.GET:
            context['is_popup'] = True
        if 'admlink' in request.GET:
            context['admlink'] = True
        return django.shortcuts.render(request, 'bots/detail.html', context)


@login_required
@permission_required('bots.view_ta', login_url='/')
def confirm(request):
    context = {'title': _('Confirmations')}
    if request.method == 'GET':
        if IS_POPUP_VAR in request.GET:
            context['is_popup'] = True
        cleaned_data = viewlib.load_selection(request, forms.SelectConfirm)
        if cleaned_data:
            cleaned_data.update({k: v for k, v in request.GET.items()})
            if cleaned_data.get(IS_POPUP_VAR):
                context['is_popup'] = True
            context['form'] = formin = forms.SelectConfirm(cleaned_data, request=request)
            if 'select' in request.GET or not formin.is_valid():
                return django.shortcuts.render(request, formin.template, context)
            cleaned_data = formin.cleaned_data

        elif 'select' in request.GET:
            # from menu:select->confirm
            context['form'] = form = forms.SelectConfirm(request=request)
            if len(request.GET) > 1:
                form.initial.update(form.initials)
                form.initial.update({k: v for k, v in request.GET.items()})
                context['form'] = form = forms.SelectConfirm(form.initial, request=request)
                form.is_valid()
            return django.shortcuts.render(request, form.template, context)

        else:
            # from menu:run->confirm
            context['form'] = form = forms.ViewConfirm(request=request)
            cleaned_data = form.initials
            if len(request.GET) > 0:
                form.initial.update(form.initials)
                form.initial.update({k: v for k, v in request.GET.items()})
                context['form'] = formin = forms.ViewConfirm(form.initial, request=request)
                if not formin.is_valid():
                    return django.shortcuts.render(request, formin.template, context)
                if formin.cleaned_data.get('idta'):
                    cleaned_data.update({
                        'idta': formin.cleaned_data['idta'],
                        'datefrom': '', 'dateuntil': '', 'confirmed': '',
                        'admlink': formin.cleaned_data['admlink'],
                        'popup': formin.cleaned_data['popup'],
                    })
                else:
                    cleaned_data = formin.cleaned_data
                cleaned_data['confirmed'] = formin.cleaned_data['confirmed'] or ''
        # go to default confirm-query using these default parameters

    else:
        # request.method == 'POST'

        # is_popup
        if request.POST.get(IS_POPUP_VAR):
            context['is_popup'] = True

        # save/delete selection
        if request.POST.get('selection_name') \
                or 'saveselection' in request.POST \
                or 'deleteselection' in request.POST:
            # from SelectConfirm form
            context['form'] = formin = forms.SelectConfirm(request.POST, request=request)
            if not formin.is_valid():
                return django.shortcuts.render(request, formin.template, context)
            if 'deleteselection' in request.POST:
                # delete selection
                viewlib.delete_selection(formin)
            else:
                # Save selection
                viewlib.save_selection(formin)
            if not 'fromselect' in request.POST:
                context['form'] = form = forms.SelectConfirm(formin.cleaned_data, request=request)
                return django.shortcuts.render(request, form.template, context)
        else:
            context['form'] = formin = forms.ViewConfirm(request.POST, request=request)

        # from ViewConfirm form, check this form first
        if not formin.is_valid():
            return django.shortcuts.render(request, formin.template, context)

        if '2select' in request.POST:
            # coming from ViewConfirm, change the selection criteria, go to select form
            context['form'] = form = forms.SelectConfirm(formin.cleaned_data, request=request)
            return django.shortcuts.render(request, form.template, context)
        if '2incoming' in request.POST:
            # coming from ViewConfirm, go to incoming form using same criteria
            request.POST = viewlib.changepostparameters(request.POST, soort='confirm2in')
            return incoming(request)
        if '2outgoing' in request.POST:
            # coming from ViewConfirm, go to outgoing form using same criteria
            request.POST = viewlib.changepostparameters(request.POST, soort='confirm2out')
            return outgoing(request)
        if 'permlink' in request.POST:
            # Redirect to permanent url
            return django.shortcuts.redirect(viewlib.get_selection_url(formin))

        action, idtas = None, []
        if 'action' in request.POST and request.POST.get('actions'):
            action = request.POST.get('actions')
            idtas = [viewlib.safe_int(i) for i in request.POST.getlist('sel')]
        elif 'confirm' in request.POST:
            action = 'confirm'
            idtas = [viewlib.safe_int(request.POST['confirm'])]
        if action == 'confirm':
            if request.user.has_perm('bots.change_ta'):
                confirmed = []
                impossible = []
                for idta in idtas:
                    ta_object = get_object_or_404(models.ta, idta=idta)
                    if not ta_object.confirmidta and ta_object.confirmtype.startswith('ask'):
                        ta_object.confirmed = True
                        # to indicate a manual confirmation
                        ta_object.confirmidta = '-1'
                        ta_object.save()
                        confirmed.append(idta)
                    else:
                        impossible.append(idta)
                if confirmed:
                    messages.add_message(
                        request, messages.INFO, _('Manual confirmed: %s') % str(confirmed)[1:-1])
                if impossible:
                    messages.add_message(
                        request, messages.ERROR, _('Manual confirm not possible: %s') % str(impossible)[1:-1])

        # coming from ViewConfirm, next page etc
        viewlib.handlepagination(request.POST, formin)
        cleaned_data = formin.cleaned_data

    # normal confirm-query with parameters
    query = models.ta.objects.filter(confirmasked=True)
    context['queryset'] = viewlib.filterquery(query, cleaned_data, context=context)
    context['form'] = form = forms.ViewConfirm(initial=cleaned_data, request=request)
    if cleaned_data.get('confirmidta') == -1:
        context['title'] += ' - %s' % _('Manual')
    elif cleaned_data.get('confirmidta'):
        context['title'] += ': %s' % cleaned_data.get('confirmidta')
    elif cleaned_data.get('confirmidta') == 0:
        context['title'] += ' - %s' % _('Pending ...')
    elif cleaned_data.get('confirmed') == '0':
        context['title'] += ' - %s' % _('Unconfirmed')
    elif cleaned_data.get('confirmed') == '1':
        context['title'] += ' - %s' % _('Confirmed')
    return django.shortcuts.render(request, form.template, context)


@login_required
@permission_required('bots.view_ta', login_url='/')
def filer(request):
    """handles bots file viewer. Only files in data dir of Bots are displayed."""
    if request.method == 'GET':
        try:
            idta = request.GET['idta']
            if idta == 0:
                # for the 'starred' file names (eg multiple output)
                raise Exception('to be caught')

            currentta = models.ta.objects.filter(idta=idta).first()
            if not currentta:
                return django.shortcuts.render(
                    request, 'bots/filer.html', {'error_content': _('No such file.')})
            # Download
            if request.GET['action'] == 'downl':
                ext = '.txt'
                if currentta.editype == 'edifact' or currentta.contenttype == 'application/edifact':
                    ext = '.edi'
                elif currentta.editype == 'x12':
                    ext = '.x12'
                elif currentta.editype in ['xml', 'xmlnocheck'] \
                        or currentta.contenttype in ['text/xml', 'application/xml']:
                    ext = '.xml'
                elif currentta.editype in ['json', 'jsonnocheck'] \
                        or currentta.contenttype == 'application/json':
                    ext = '.json'
                elif currentta.contenttype == 'text/html':
                    ext = '.html'
                elif currentta.contenttype == 'application/pdf':
                    ext = '.pdf'
                elif currentta.editype == 'csv':
                    ext = '.csv'
                elif currentta.editype == 'idoc':
                    ext = '.idoc'
                elif currentta.editype == 'excel':
                    ext = '.xls'

                # filename
                filename = currentta.filename + ext
                if currentta.status == EXTERNIN:
                    filename = os.path.basename(currentta.filename)
                    # EXTERNIN has no file, so go to first FILEIN
                    currentta = models.ta.objects.filter(parent=currentta.idta).first()
                elif currentta.status == FILEIN and currentta.divtext:
                    # email attachment filename
                    filename = currentta.divtext
                elif currentta.status == EXTERNOUT:
                    filename = os.path.basename(currentta.filename)
                    # EXTERNOUT has no file, so go to last FILEOUT
                    currentta = models.ta.objects.filter(idta=currentta.parent).first()
                if currentta.contenttype in [
                        'message/rfc822', 'multipart/mixed'] and currentta.reference:
                    # email (s)mime
                    filename = '%s.eml' % currentta.reference.strip('<>')

                # botsglobal.logger.debug('Downloaded filename: %s', currentta.filename)
                response = django.http.HttpResponse(content_type=currentta.contenttype)
                dispositiontype = 'attachment'
                response['Content-Disposition'] = (
                    '%s; filename=%s' % (dispositiontype, filename))
                # ~ response['Content-Length'] = os.path.getsize(absfilename)
                response.write(botslib.readdata_bin(currentta.filename))
                return response

            if request.GET['action'] == 'previous':
                if currentta.parent:
                    # has a explicit parent
                    talijst = list(models.ta.objects.filter(idta=currentta.parent))
                else:
                    # get list of ta's referring to this idta as child
                    talijst = list(
                        models.ta.objects.filter(
                            idta__range=(currentta.script, currentta.idta), child=currentta.idta
                        )
                    )
            elif request.GET['action'] == 'this':
                if currentta.status == EXTERNIN:
                    # EXTERNIN can not be displayed, so go to first FILEIN
                    talijst = list(models.ta.objects.filter(parent=currentta.idta))
                elif currentta.status == EXTERNOUT:
                    # EXTERNOUT can not be displayed, so go to last FILEOUT
                    talijst = list(models.ta.objects.filter(idta=currentta.parent))
                else:
                    talijst = [currentta]
            elif request.GET['action'] == 'next':
                if currentta.child:
                    # has a explicit child
                    talijst = list(models.ta.objects.filter(idta=currentta.child))
                else:
                    talijst = list(models.ta.objects.filter(parent=currentta.idta))
            for ta_object in talijst:
                # determine if file could be displayed
                if ta_object.filename and ta_object.filename.isdigit():
                    if ta_object.contenttype == 'application/pdf':
                        ta_object.content = str(b64encode(
                            botslib.readdata_bin(ta_object.filename)
                        ))[2:-1]
                    elif ta_object.charset:
                        ta_object.content = botslib.readdata(
                            ta_object.filename, charset=ta_object.charset, errors='ignore'
                        )
                    else:
                        # guess safe choice for charset.
                        # alt1: get charset by looking forward (until translation).
                        # alt2: try with utf-8, if error iso-8859-1
                        ta_object.content = botslib.readdata(
                            ta_object.filename, charset='us-ascii', errors='ignore'
                        )
                    ta_object.has_file = True
                    if ta_object.editype == 'x12':
                        ta_object.content = viewlib.indent_x12(ta_object.content)
                    elif ta_object.editype == 'edifact':
                        ta_object.content = viewlib.indent_edifact(ta_object.content)
                else:
                    ta_object.has_file = False
                    ta_object.content = _('No file available for display.')
                # determine has previous:
                if ta_object.parent or ta_object.status == MERGED:
                    ta_object.has_previous = True
                else:
                    ta_object.has_previous = False
                # determine: has next:
                if ta_object.status == EXTERNOUT or ta_object.statust in [OPEN, ERROR]:
                    ta_object.has_next = False
                else:
                    ta_object.has_next = True
            channels = [ta_object.fromchannel, ta_object.tochannel]
            channels_types = models.get_channels_types(idchannel__in=channels) if channels else {}
            return django.shortcuts.render(
                request, 'bots/filer.html', {
                    'idtas': talijst,
                    'display': request.GET.get('display', ''),
                    'admlink': True,
                    'partners': models.getactivepartners(),
                    'inactive_partners': models.getinactivepartners(),
                    'channels_types': channels_types,
                })
        except:
            return django.shortcuts.render(
                request, 'bots/filer.html', {'error_content': _('No such file.')})


@login_required
@permission_required('bots.change_translate', login_url='/')
def srcfiler(request):
    """handles bots source file viewer. display grammar, mapping, userscript etc."""
    if request.method == 'GET':
        try:
            src = request.GET.get('src')
            if not src:
                raise Http404()
            srcfile = os.path.abspath(
                os.path.join(botsglobal.ini.get('directories', 'usersysabs'), src))
            # Get only python files in usersys !
            if not srcfile.startswith(botsglobal.ini.get('directories', 'usersysabs')):
                botsglobal.logger.error('Invalid source file requested: %s', src)
                raise Http404()
            if srcfile.endswith('.py') and os.path.isfile(srcfile):
                max_size = botsglobal.ini.getint('webserver', 'max_src_size', 5000000)
                if os.stat(srcfile).st_size > max_size:
                    err = _('File too big exceed %(max_size)s: %(src)s') % {
                        'max_size': max_size, 'src': src}
                    botsglobal.logger.error(err)
                    return django.shortcuts.render(
                        request, 'bots/srcfiler.html', {'error_content': err}
                    )
                source = open(srcfile).read()
                classified_text = py2html.analyze_python(source)
                html_source = py2html.html_highlight(classified_text)
                return django.shortcuts.render(
                    request, 'bots/srcfiler.html', {'src': src, 'html_source': html_source}
                )
            return django.shortcuts.render(
                request,
                'bots/srcfiler.html',
                {'error_content': _('File not found: %s') % src},
            )
        except:
            return django.shortcuts.render(
                request, 'bots/srcfiler.html', {'error_content': _('No such file.')}
            )


@login_required
@permission_required('bots.view_report', login_url='/')
def logfiler(request):
    """handles bots log file viewer. display/download any file in logging directory."""
    if request.method == 'GET':
        logname = ''
        if 'engine' in request.GET:
            logname = 'engine'
        elif 'jobqueue' in request.GET:
            logname = 'jobqueue'
        elif 'dirmonitor' in request.GET:
            logname = 'dirmonitor'
        elif 'webserver' in request.GET:
            logname = 'webserver'
        context = {
            'logname': logname,
            'is_popup': True,
        }
        logdir = botsglobal.ini.get('directories', 'logging')
        if not logname:
            context['logfiles'] = sorted(os.listdir(logdir), key=lambda s: s.lower())
        else:
            logdata = ''
            logdir = botslib.join(logdir, logname)
            logfiles = glob.glob(os.path.join(logdir, '*'))
            logfiles.sort(key=os.path.getmtime, reverse=True)
            if logname == 'engine' and botsglobal.ini.get(
                    'settings', 'log_when', None) == 'report':
                logfiles = logfiles[:25]
            context['logfiles'] = [os.path.basename(l) for l in logfiles]
            log = request.GET.get(logname, '').split(os.sep)[-1]
            if not log:
                if logname == 'engine':
                    log = str(viewlib.get_reportidta(request.GET) or '')
                if not log and context['logfiles']:
                    log = context['logfiles'][0]
            if log:
                context['log'] = log
                logfile = botslib.join(logdir, log)
                if os.path.isfile(logfile):
                    try:
                        logdata = open(logfile).read()
                        context['logdata'] = logdata or _('Empty log file.')
                    except Exception as exc:
                        err = _('Error occured when reading log %(log)s: %(exc)s') % locals()
                        botsglobal.logger.error(err)
                        context['logdata'] = err
                else:
                    context['logdata'] = _('No such file.')
                    if botsglobal.settings.DEBUG:
                        context['logdata'] += ': %s' % logfile

            # Download log file
            if request.GET.get('action') == 'download' and logdata:
                if logname == 'engine':
                    filename = 'bots-%s_%s.log' % (logname, log)
                else:
                    filename = 'bots-%s' % log
                if filename.endswith('.log'):
                    sts = os.stat(logfile)
                    mdt = datetime.fromtimestamp(sts.st_mtime)
                    filename = '%s_%s.log' % (filename[:-4], mdt.strftime('%Y%m%d_%H%M'))
                else:
                    filename = '%s.log' % filename.replace('.log', '')
                response = django.http.HttpResponse(content_type='text/log')
                response['Content-Disposition'] = 'attachment; filename=%s' % filename
                response.write(logdata)
                return response

        return django.shortcuts.render(request, 'bots/logfiler.html', context)


@login_required
@user_passes_test(lambda u: u.is_superuser, login_url='/')
def plugin(request):
    """Load bots plugin"""
    if request.method == 'GET':
        form = forms.UploadFileForm()
        return django.shortcuts.render(request, 'bots/plugin.html', {'form': form})

    if 'submit' in request.POST:
        formin = forms.UploadFileForm(request.POST, request.FILES)
        if formin.is_valid():
            # write backup plugin first
            plugout_backup_core(request)
            # read the plugin
            try:
                if pluglib.read_plugin(request.FILES['file'].temporary_file_path()):
                    messages.add_message(request, messages.INFO, _('Overwritten existing files.'))
            except Exception as exc:
                notification = _('Error occured while reading plugin "%(name)s": %(exc)s') % {
                    'name': request.FILES['file'].name, 'exc': exc}
                botsglobal.logger.error(notification)
                messages.add_message(request, messages.ERROR, notification)
                request.FILES['file'].close()
                return django.shortcuts.render(request, 'bots/plugin.html', {'form': formin})
            else:
                notification = _('Plugin "%s" is read successful.') % request.FILES['file'].name
                botsglobal.logger.info(notification)
                messages.add_message(request, messages.INFO, notification)
            finally:
                request.FILES['file'].close()
                # seems to be needed according to django docs.
        else:
            messages.add_message(request, messages.INFO, _('No plugin read.'))
    return django.shortcuts.redirect('bots:home')


@login_required
@user_passes_test(lambda u: u.is_superuser, login_url='/')
def plugin_index(request):
    """ """
    if request.method == 'GET':
        return django.shortcuts.render(request, 'bots/plugin_index.html')

    if 'submit' in request.POST:
        # write backup plugin first
        plugout_backup_core(request)
        # read the plugin
        try:
            pluglib.read_index('index')
        except Exception as exc:
            notification = _('Error occured while reading configuration index file: "%s".') % unicode(exc)
            botsglobal.logger.error(notification)
            messages.add_message(request, messages.ERROR, notification)
        else:
            notification = _('Configuration index file is read successful.')
            botsglobal.logger.info(notification)
            messages.add_message(request, messages.INFO, notification)
    return django.shortcuts.redirect('bots:home')


@login_required
@user_passes_test(lambda u: u.is_superuser, login_url='/')
def plugout_index(request):
    if request.method == 'GET':
        filename = botslib.join(botsglobal.ini.get('directories', 'usersysabs'), 'index.py')
        botsglobal.logger.info(
            _('Start writing configuration index file "%(file)s".'), {'file': filename}
        )
        try:
            dummy_for_cleaned_data = {
                'databaseconfiguration': True,
                'umlists': botsglobal.ini.getboolean('settings', 'codelists_in_plugin', True),
                'databasetransactions': False,
            }
            pluglib.make_index(dummy_for_cleaned_data, filename)
        except Exception as exc:
            notification = _('Error writing configuration index file: "%s".') % unicode(exc)
            botsglobal.logger.error(notification)
            messages.add_message(request, messages.ERROR, notification)
        else:
            notification = _('Configuration index file "%s" is written successful.') % filename
            botsglobal.logger.info(notification)
            messages.add_message(request, messages.INFO, notification)
        return django.shortcuts.redirect('bots:home')


@login_required
@user_passes_test(lambda u: u.is_superuser, login_url='/')
def plugout_backup(request):
    if request.method == 'GET':
        plugout_backup_core(request)
    return django.shortcuts.redirect('bots:home')


def plugout_backup_core(request):
    filename = botslib.join(
        botsglobal.ini.get('directories', 'botssys'),
        'backup_plugin_%s.zip' % time.strftime('%Y%m%d%H%M%S'),
    )
    botsglobal.logger.info(_('Start writing backup plugin "%(file)s".'), {'file': filename})
    try:
        dummy_for_cleaned_data = {
            'databaseconfiguration': True,
            'umlists': botsglobal.ini.getboolean('settings', 'codelists_in_plugin', True),
            'fileconfiguration': True,
            'infiles': False,
            'charset': True,
            'databasetransactions': False,
            'data': False,
            'logfiles': False,
            'config': False,
            'database': False,
        }
        pluglib.make_plugin(dummy_for_cleaned_data, filename)
    except Exception as exc:
        notification = _('Error writing backup plugin: "%s".') % unicode(exc)
        botsglobal.logger.error(notification)
        messages.add_message(request, messages.ERROR, notification)
    else:
        notification = _('Backup plugin "%s" is written successful.') % filename
        botsglobal.logger.info(notification)
        messages.add_message(request, messages.INFO, notification)


@login_required
@user_passes_test(lambda u: u.is_superuser, login_url='/')
def plugout(request):
    """Create bots plugins."""
    context = {}
    if request.method == 'GET':
        context['form'] = form = forms.PlugoutForm()
        return django.shortcuts.render(request, form.template, context)

    if 'submit' in request.POST:
        formin = forms.PlugoutForm(request.POST)
        if formin.is_valid():
            filename = 'plugin_bots_%s.zip' % time.strftime('%Y%m%d%H%M%S')
            filename = botslib.join(botsglobal.ini.get('directories', 'botssys'), filename)
            botsglobal.logger.info(_('Start writing plugin "%(file)s".'), {'file': filename})
            try:
                pluglib.make_plugin(formin.cleaned_data, filename)
            except botslib.PluginError as exc:
                botsglobal.logger.error(exc)
                messages.add_message(request, messages.ERROR, exc)
            else:
                botsglobal.logger.info(
                    _('Plugin "%(file)s" created successful.'), {'file': filename}
                )
                response = django.http.HttpResponse(
                    open(filename, 'rb').read(), content_type='application/zip'
                )
                # response['Content-Length'] = os.path.getsize(filename)
                response['Content-Disposition'] = 'attachment; filename=%s'
                response['Content-Disposition'] %= os.path.basename(filename)
                os.remove(filename)
                return response
    return django.shortcuts.redirect('bots:home')


@login_required
@user_passes_test(lambda u: u.is_superuser, login_url='/')
def delete(request):
    """Bots delete view"""
    if request.method == 'GET':
        form = forms.DeleteForm()
        return django.shortcuts.render(request, 'bots/delete.html', {'form': form})

    if 'submit' in request.POST:
        form = forms.DeleteForm(request.POST)
        if form.is_valid():
            if form.cleaned_data.get('delconfiguration') \
                    or form.cleaned_data.get('delcodelists') \
                    or form.cleaned_data.get('deluserscripts'):
                # write backup plugin first
                plugout_backup_core(request)
            botsglobal.logger.info(_('Start deleting in configuration.'))
            if form.cleaned_data.get('deltransactions'):
                # while testing with very big loads,
                # deleting gave error. Using raw SQL solved this.
                with transaction.atomic():
                    cursor = connection.cursor()
                    cursor.execute("""DELETE FROM ta""")
                    cursor.execute("""DELETE FROM filereport""")
                    cursor.execute("""DELETE FROM report""")
                messages.add_message(request, messages.INFO, _('Transactions are deleted.'))
                botsglobal.logger.info(_('Transactions are deleted.'))
                # clean engine logs
                if botsglobal.ini.get('settings', 'log_file_number', None) == 'idta':
                    logdir = botslib.join(botsglobal.ini.get('directories', 'logging'), 'engine')
                    if os.access(logdir, os.W_OK):
                        shutil.rmtree(logdir, ignore_errors=True)
                # clean data files
                deletefrompath = botsglobal.ini.get('directories', 'data', 'botssys/data')
                shutil.rmtree(deletefrompath, ignore_errors=True)
                botslib.dirshouldbethere(deletefrompath)
                notification = _('Data files are deleted.')
                messages.add_message(request, messages.INFO, notification)
                botsglobal.logger.info(notification)
            elif form.cleaned_data.get('delacceptance'):
                # list of files for deletion in data-directory
                list_file = []
                report_idta_lowest = 0
                # for each acceptance report. is not very efficient.
                for acc_report in models.report.objects.filter(acceptance=1):
                    if not report_idta_lowest:
                        report_idta_lowest = acc_report.idta
                    # select 'next' report...
                    max_report_idta = models.report.objects.filter(
                        idta__gt=acc_report.idta
                    ).aggregate(Min('idta'))['idta__min']
                    if not max_report_idta:
                        # if report is report of last run, there is no next report
                        max_report_idta = sys.maxsize
                    # we have a idta-range now: from (and including) acc_report.idta
                    # till (and excluding) max_report_idta
                    list_file += (
                        models.ta.objects.filter(
                            idta__gte=acc_report.idta, idta__lt=max_report_idta
                        )
                        .exclude(status=1)
                        .values_list('filename', flat=True)
                        .distinct()
                    )
                    # delete ta in range
                    models.ta.objects.filter(
                        idta__gte=acc_report.idta, idta__lt=max_report_idta
                    ).delete()
                    # delete filereports in range
                    models.filereport.objects.filter(
                        idta__gte=acc_report.idta, idta__lt=max_report_idta
                    ).delete()
                if report_idta_lowest:
                    # delete all acceptance reports
                    models.report.objects.filter(
                        idta__gte=report_idta_lowest, acceptance=1
                    ).delete()
                    # delete all files in data directory geenrated during acceptance testing
                    for filename in list_file:
                        if filename.isdigit():
                            botslib.deldata(filename)
                notification = _('Transactions from acceptance-testing deleted.')
                messages.add_message(request, messages.INFO, notification)
                botsglobal.logger.info(notification)
            if form.cleaned_data.get('delconfiguration'):
                models.confirmrule.objects.all().delete()
                models.routes.objects.all().delete()
                models.channel.objects.all().delete()
                models.chanpar.objects.all().delete()
                models.translate.objects.all().delete()
                models.partner.objects.all().delete()
                notification = _('Database configuration is deleted.')
                messages.add_message(request, messages.INFO, notification)
                botsglobal.logger.info(notification)
            if form.cleaned_data.get('delcodelists'):
                # while testing with very big loads, deleting gave error.
                # Using raw SQL solved this.
                with transaction.atomic():
                    cursor = connection.cursor()
                    cursor.execute("""DELETE FROM ccode""")
                    cursor.execute("""DELETE FROM ccodetrigger""")
                notification = _('User code lists are deleted.')
                messages.add_message(request, messages.INFO, notification)
                botsglobal.logger.info(notification)
            if form.cleaned_data.get('delpersist'):
                with transaction.atomic():
                    cursor = connection.cursor()
                    cursor.execute("""DELETE FROM persist""")
                notification = _('Persist data is deleted.')
                messages.add_message(request, messages.INFO, notification)
                botsglobal.logger.info(notification)
            if form.cleaned_data.get('delinfile'):
                deletefrompath = botslib.join(
                    botsglobal.ini.get('directories', 'botssys', 'botssys'), 'infile'
                )
                shutil.rmtree(deletefrompath, ignore_errors=True)
                notification = _('Files in botssys/infile are deleted.')
                messages.add_message(request, messages.INFO, notification)
                botsglobal.logger.info(notification)
            if form.cleaned_data.get('deloutfile'):
                deletefrompath = botslib.join(
                    botsglobal.ini.get('directories', 'botssys', 'botssys'), 'outfile'
                )
                shutil.rmtree(deletefrompath, ignore_errors=True)
                notification = _('Files in botssys/outfile are deleted.')
                messages.add_message(request, messages.INFO, notification)
                botsglobal.logger.info(notification)
            if form.cleaned_data.get('deluserscripts'):
                deletefrompath = botsglobal.ini.get('directories', 'usersysabs')
                for root, dirs, files in os.walk(deletefrompath):
                    head, tail = os.path.split(root)
                    if tail == 'charsets':
                        del dirs[:]
                        continue
                    for bestand in files:
                        if bestand != '__init__.py':
                            os.remove(os.path.join(root, bestand))
                notification = _('User scripts are deleted (in usersys).')
                messages.add_message(request, messages.INFO, notification)
                botsglobal.logger.info(notification)
            botsglobal.logger.info(_('Finished deleting in configuration.'))
    return django.shortcuts.redirect('bots:home')


@login_required
@permission_required('bots.change_mutex', login_url='/')
def runengine(request):
    """
    Start bots-engine from request parameters

    cmd arguments:
        1. bots-engine
        2. environment (config).
        3. commandstorun (eg --new) and routes.
    """
    if request.method == 'GET':
        environment = '-c' + botsglobal.ini.get('directories', 'config_org')
        lijst = ['bots-engine', environment]
        # get 3. commandstorun (eg --new) and routes via request
        if request.GET.get('clparameter'):
            lijst.append(request.GET['clparameter'])

        # either bots-engine is run directly or via jobqueue-server
        if botsglobal.ini.getboolean('jobqueue', 'enabled', False):
            # run bots-engine via jobqueue-server; reports back if job is queued
            terug = job2queue.send_job_to_jobqueue(lijst)
            txt = '%s: %s' % (job2queue.JOBQUEUEMESSAGE2TXT[terug], lijst[-1])
            if terug == 0:
                messages.add_message(request, messages.INFO, txt)
                botsglobal.logger.info(txt)
            elif terug == 1:
                messages.add_message(request, messages.ERROR, txt)
                botsglobal.logger.error(txt)
            else:
                messages.add_message(request, messages.WARNING, txt)
                botsglobal.logger.warning(txt)
        else:
            # run bots-engine direct.
            # reports back if bots-engine is started succesful.
            # **not reported: problems with running.
            botsglobal.logger.info(
                _('Run bots-engine with parameters: "%(parameters)s"'),
                {'parameters': unicode(lijst)},
            )

            # first check if another instance of bots-engine is running/if port is free
            try:
                engine_socket = botslib.check_if_other_engine_is_running()
            except socket.error:
                notification = _(
                    'Trying to run "bots-engine",'
                    ' but another instance of "bots-engine" is running.'
                    ' Please try again later.'
                )
                messages.add_message(request, messages.ERROR, notification)
                botsglobal.logger.error(notification)
                return django.shortcuts.redirect('bots:home')
            else:
                # Close the socket
                engine_socket.close()

            # run engine
            try:
                terug = subprocess.Popen(lijst).pid
            except Exception as exc:
                notification = _('Errors while trying to run bots-engine: "%s".') % exc
                messages.add_message(request, messages.ERROR, notification)
                botsglobal.logger.error(notification)
            else:
                messages.add_message(request, messages.INFO, _('Bots-engine is started.'))

        if request.user.has_perm('bots.view_report'):
            return django.shortcuts.redirect('bots:reports')

    return django.shortcuts.redirect('bots:home')


@login_required
@user_passes_test(lambda u: u.is_superuser, login_url='/')
def sendtestmailmanagers(request):
    try:
        sendornot = botsglobal.ini.getboolean('settings', 'sendreportiferror', False)
    except botslib.BotsError:
        sendornot = False
    if not sendornot:
        notification = _(
            'Trying to send test mail, but in bots.ini, section [settings], '
            '"sendreportiferror" is not "True".'
        )
        botsglobal.logger.info(notification)
        messages.add_message(request, messages.ERROR, notification)
        return django.shortcuts.redirect('bots:home')

    try:
        mail_managers(_('testsubject'), _('test content of report'))
    except:
        txt = botslib.txtexc()
        messages.add_message(request, messages.ERROR, _('Sending test mail failed.'))
        botsglobal.logger.info(_('Sending test mail failed, error:\n%(txt)s'), {'txt': txt})
        return django.shortcuts.redirect('bots:home')

    notification = _('Sending test mail succeeded.')
    messages.add_message(request, messages.INFO, notification)
    botsglobal.logger.info(notification)
    return django.shortcuts.redirect('bots:home')
