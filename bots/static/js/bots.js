/*!
* Bots ui javascript functions
*/

function _popup(href, width=1200, height=600, name='') {
    window.open(href, name||href, 'width=' + width + ', height=' + height);
}


function popup(elm, width=1200, height=600, args='') {
    _popup(elm.href + args, width, height, elm.name);
    return false;
}


function add_shortcut(elem) {
    var pathargs = window.location.href.split(window.location.pathname)[1]
    var shortcut_url = window.location.pathname + pathargs;
    var paths = shortcut_url.split('/');
    if (paths[paths.length - 1]) {
        var shortcut_name = paths[paths.length - 1];
        if (shortcut_name[0] == '?') {
            shortcut_name = paths[paths.length - 2] + shortcut_name;
        }
    } else {
        var shortcut_name = paths[paths.length - 2];
    }
    shortcut_name = shortcut_name.replace('?', ' ').replace('&', ' ');
    var shortcut_name = prompt('Shortcut url: ' + shortcut_url + '\nName:', shortcut_name);
    if (!shortcut_name) {
        return false;
    }
    elem.href += '?ccodeid=shortcut';
    elem.href += '&rightcode=' + encodeURIComponent(shortcut_url);
    elem.href += '&leftcode=' + encodeURIComponent(shortcut_name);
    if (elem.getAttribute('attr8')) {
        elem.href += '&attr8=' + encodeURIComponent(elem.getAttribute('attr8'));
    }
    elem.href += '&attr2=custom';
}
