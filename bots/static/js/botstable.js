/*!
* Botstable javascript functions
*/

/* Botstable actions */
function sorted() {
    sortedby = document.getElementById('id_sortedby').value;
    if (sortedby) {
        th_sorted = document.getElementById('th_' + sortedby);
        if (!th_sorted) {
            return false;
        }
        th_sorted.classList.add("sortedby");
        if (document.getElementById('id_sortedasc').value === 'True') {
            // sort_tag = "▲";
	    var cssclass = 'sortedasc';
        } else {
            // sort_tag = "▼";
	    var cssclass = 'sorted';
        };
        var div = document.createElement('div');
        div.innerHTML = '<button class="botsheader ico-del-ha ' + cssclass + '" type="submit" name="order" value="" title="Clear sorting"></button>';
        var botsheader = th_sorted.getElementsByClassName("botsheader");
        botsheader[0].parentElement.insertBefore(div.firstChild, botsheader[0]);
    }
}


function get_selected(name='sel') {
    return document.querySelectorAll('input[name=' + name + ']:checked');
}


function update_selcount() {
    var selected = get_selected();
    var selcount = document.getElementById('selcount');
    if (selected.length) {
        selcount.innerHTML = '(' + selected.length + ')';
    } else {
        selcount.innerHTML = '';
    }
}


function checktrcls(checkbox, cls='filteredfield') {
    var tr = get_parent_tr(checkbox);
    if (checkbox.checked) {
        tr.classList.add(cls);
    } else {
        tr.classList.remove(cls);
    }
}

function checktr(checkbox, cls='filteredfield') {
    checktrcls(checkbox, cls);
    update_selcount();
}

function checkall(checkbox, name) {
    var checkboxes = document.getElementsByName(name);
    checkboxes.forEach(function(elem) {
        elem.checked = checkbox.checked;
        // add filteredfield class to parent TR
        checktrcls(elem);
    });
    update_selcount();
}


function confirmaction() {
    var actions = document.getElementById('id_actions');
    if (actions.value) {
        if (['delete', 'confirm'].indexOf(actions.value) == -1) {
            return true;
        }
        var selected = get_selected();
        var idtas = '';
        selected.forEach(function(sel) {
            idtas += sel.value + ', ';
        });
        if (idtas.length) {
            return confirm(actions.selectedOptions[0].innerText + ' (' + selected.length + ') : ' + idtas.slice(0, -2));
        }
    }
    return false;
}


/* Botstable query */
function get_parent(nodename, elem) {
    while (elem.nodeName != nodename) {
        elem = elem.parentNode;
    }
    return elem;
}


function get_parent_tr(elem) {
    return get_parent('TR', elem);
}


function get_parent_td(elem) {
    // while (!elem.classList.contains('dropdown')) {
    return get_parent('TD', elem);
}


function get_value(elem) {
    return elem.innerText || get_parent_td(elem).innerText;
}


function filterfield(elem, id, prefix='') {
    var value = get_value(elem);
    var filter = document.getElementById(id);
    if (filter.options) {
        filter.value = prefix + value;
    } else {
        filter.value += ' ' + prefix + value;
    }
}
