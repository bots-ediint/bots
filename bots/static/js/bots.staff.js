/* Bots staff & admin */

function view_partner(elem, width=800, height=600, args='?_popup') {
    if (!elem.href) {
        var idpartner = get_value(elem);
        elem.href = url_bots_partner + idpartner + '/change/' + args;
    }
    _popup(elem.href, width, height);
    return false;
}


function add_partner(elem, width=800, height=600, args='&_popup') {
    if (!elem.href) {
        var idpartner = get_value(elem);
        elem.href = url_bots_partner_add + '?idpartner=' + idpartner + '&name=' + idpartner + args;
    }
    _popup(elem.href, width, height);
    return false;
}


function view_channel(elem, width=800, height=600, args='?_popup') {
    if (!elem.href) {
        var idchannel = get_value(elem);
        elem.href = url_bots_channel + idchannel + '/change/' + args;
    }
    _popup(elem.href, width, height);
    return false;
}


function view_route(elem, args='') {
    if (!elem.href) {
        var idroute = get_value(elem);
        elem.href = url_bots_routes + '?idroute=' + idroute + args;
        elem.target = '_blank';
    }
}


function view_translate(elem, args='') {
    if (elem.href) {
        return;
    }
    elem.href = url_bots_translate;
    elem.target = '_blank';
    var td = get_parent_td(elem);
    var tr = td.parentNode;
    if (td.getAttribute('name').substring(0, 3) == 'out') {
        var outeditype = tr.querySelector("[name='outeditype']");
        var outmessagetype = tr.querySelector("[name='outmessagetype']");
        elem.href += '?toeditype__exact=' + outeditype.innerText;
        if (td.getAttribute('name') == 'outmessagetype') {
            elem.href += '&tomessagetype=' + outmessagetype.innerText;
        }
        return;
    }
    if (td.getAttribute('name').substring(0, 2) == 'in'|| td.getAttribute('name') == 'divtext') {
        var ineditype = tr.querySelector("[name='ineditype']");
        var inmessagetype = tr.querySelector("[name='inmessagetype']");
    } else {
        var editype = tr.querySelector("[name='editype']");
        var messagetype = tr.querySelector("[name='messagetype']");
    }
    var tscript = tr.querySelector("[name='divtext']");
    // fromeditype (ineditype)
    elem.href += '?fromeditype__exact=' + ineditype.innerText;
    // frommessagetype (inmessagetype)
    if (td != ineditype) {
        elem.href += '&frommessagetype=' + inmessagetype.innerText;
    }
    if (td == tscript) {
        elem.href += '&tscript=' + tscript.innerText;
    }
    elem.href += args
}


function get_msgtype(elem) {
    var td = get_parent_td(elem);
    var tr = td.parentNode;
    if (td.getAttribute('name').substring(0, 2) == 'in') {
        var editype = tr.querySelector("[name='ineditype']");
        var messagetype = tr.querySelector("[name='inmessagetype']");
    } else if (td.getAttribute('name').substring(0, 3) == 'out') {
        var editype = tr.querySelector("[name='outeditype']");
        var messagetype = tr.querySelector("[name='outmessagetype']");
    } else {
        var editype = tr.querySelector("[name='editype']");
        var messagetype = tr.querySelector("[name='messagetype']");
    }
    if (!editype || !messagetype) {
        return;
    }
    return [editype.innerText, messagetype.innerText];
}


function view_src(elem, args='') {
    if (elem.href) {
        return;
    }
    var msgtype = get_msgtype(elem);
    elem.href = url_bots_srcfiler;
    elem.href += '/?src=grammars/' + msgtype[0] + '/' + msgtype[1] + '.py';
    elem.href += args
    elem.target = '_blank';
}
