/**
 * Bots admin javascript library
 * Copyright (C) 2021 © Ludovic WATTEAUX
**/

function insertAfter(ref, elem) {
    ref.parentNode.insertBefore(elem, ref.nextSibling);
}


function open_window(href, title) {
    if (!href) {
        return false;
    }
    if (!title) {
        title = href;
    }
    return window.open(href, title, 'width=1200, height=600');
}

function get_frommessagesrc_url() {
    return get_grammarsrc_url(fromeditype.value, frommessagetype.value);
}

function get_tomessagesrc_url() {
    return get_grammarsrc_url(toeditype.value, tomessagetype.value);
}


function create_span(elem) {
    // Add span and view source link
    var span = document.createElement("span");
    span.id = "span_" + elem.name;
    span.innerHTML = ' <a id="a_' + elem.name + '" href="#" target="_blank" class="nowrap icon-viewlink botsheader" title="View source" onclick="open_window(this.href);return false;"> View</a>';
    insertAfter(elem, span);
}

function create_select(elem) {
    var sel = document.createElement('select');
    sel.setAttribute("id", 'sel_' + elem.name);
    sel.setAttribute("onchange", 'set_' + elem.name + '(this.value);');
    sel.style.display = 'none';
    elem.parentNode.insertBefore(sel, elem);
}


function display_span(field) {
    var span = document.getElementById('span_' + field);
    var a = span.firstElementChild;
    var href = '';
    if (field == 'frommessagetype') {
        href = get_frommessagesrc_url();
    } else if (field == 'tomessagetype') {
        href = get_tomessagesrc_url();
    } else if (field == 'tscript') {
        href = get_mapping_url();
    }
    if (href) {
        a.href = href;
        span.style.display = '';
        return true;
    }
    span.style.display = 'none';
    a.href = "#";
    return false;
}

function display_span_tscript() {
    return display_span('tscript');
}

function display_span_frommessagetype() {
    return display_span('frommessagetype');
}

function display_span_tomessagetype() {
    return display_span('tomessagetype');
}

function set_tscript(script) {
    tscript.value = script;
    display_span_tscript();
}

function set_frommessagetype(msgtype) {
    frommessagetype.value = msgtype;
    display_span_frommessagetype();
    onchange_translateind();
}

function set_tomessagetype(msgtype) {
    tomessagetype.value = msgtype;
    display_span_tomessagetype();
}


function onchange_translateind() {
    var span = document.getElementById('span_translateind');
    var fieldbox_alt = document.querySelector("div.fieldBox.field-alt");
    if (translateind.value == '1') {
        var a = span.firstElementChild;
        a.href = get_translate_url() + '_popup';
        a.title = a.getAttribute('data-title') + ': ' + fromeditype.value;
        if (['edifact', 'x12'].indexOf(fromeditype.value) < 0) {
            a.title += ' ' + frommessagetype.value;
        }
        span.style.display = '';
        fieldbox_alt.style.display = '';
    } else {
        span.style.display = 'none';
        fieldbox_alt.style.display = 'none';
    }
}


function onchange_editype(editype) {
    if (editype == fromeditype) {
        var messagetype = frommessagetype;
        var sel_messagetype = sel_frommessagetype;
    } else if (editype == toeditype) {
        var messagetype = tomessagetype;
        var sel_messagetype = sel_tomessagetype;
    } else {
        return;
    }
    if (!message_types[editype.value]) {
        messagetype.style.display = '';
        sel_messagetype.style.display = 'none';
    } else {
        sel_messagetype.style.display = '';
        messagetype.style.display = 'none';
        // Clear message types options
        sel_messagetype.innerHTML = "";
        // Add message types options
        message_types[editype.value].forEach(function (msgtype) {
            var name = msgtype;
            if (!msgtype) {
                name = '------ (' + editype.value + ') ------';
            } else {
                name += ' (' + editype.value + ')';
            }
            sel_messagetype.options.add(new Option(name, msgtype));
            if (msgtype == messagetype.value) {
                sel_messagetype.value = msgtype;
            }
        });
        // Set frommessagetype
        if (sel_messagetype.value != messagetype.value) {
            messagetype.value = sel_messagetype.value;
        }
    }
    if (editype == fromeditype) {
        if (!tscript) {
            // from routes admin change_form
            sel_frommessagetype.disabled = false;
            if (['edifact', 'x12'].indexOf(fromeditype.value) > -1) {
                sel_frommessagetype.value = fromeditype.value;
                frommessagetype.value = fromeditype.value;
                sel_frommessagetype.disabled = true;
            }
            display_span_frommessagetype();
            onchange_translateind();
            return;
        }
        display_span_frommessagetype();
        // Add tscripts options
        sel_tscript.innerHTML = "";
        if (tscripts[editype.value]) {
            tscript.style.display = 'none';
            sel_tscript.style.display = '';
            tscripts[editype.value].forEach(function (script) {
                sel_tscript.options.add(new Option(script, script));
                if (script == tscript.value) {
                    sel_tscript.value = script;
                }
            });
            if (sel_tscript.value != tscript.value) {
                tscript.value = sel_tscript.value;
            }
        } else {
            tscript.style.display = '';
            sel_tscript.style.display = 'none';
        }
        display_span_tscript();
    } else if (editype == toeditype) {
        display_span_tomessagetype();
    }
}
