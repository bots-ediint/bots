"""
Bots EDI Translator __init__.py
"""
import django

if django.VERSION[0] < 4:
    default_app_config = 'bots.apps.Config'
