# -*- coding: utf-8 -*-

from django.apps import AppConfig

from . import __about__ as about


class Config(AppConfig):
    name = __package__
    verbose_name = about.__summary__

    def ready(self):
        super(Config, self).ready()
