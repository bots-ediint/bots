Bots EDI Translator Developers
==============================

Bots-ediiint Author
===================
* Ludovic Watteaux

Original Author
===============
* Henk-Jan Ebbers

Awesome Contributors
====================
* Henk-Jan Ebbers
* Mike Griffin
* Ludovic Watteaux
* Wouter Vanden Hove



See all contributions here: https://gitlab.com/bots-ediint/bots/-/graphs/master
