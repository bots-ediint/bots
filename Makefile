# Makefile for Bots open source edi translator
#
# Require mkdist submodule https://gitlab.com/lwatteaux/mkdist

TARGET_VERSION ?= 3.7.4

include mkdist/dist.mk

# python wheel
# bdist_wheel_args += --build-number=

# Red Hat (centos/fedora) .rpm
bdist_rpm_args += --source-only

# windows
bdist_wininst_args += --title="Bots open source edi translator $(VERSION)" --install-script=postinstall-bots-win.py
# bdist_msi_args +=

RELEASE_EXT := bots_ediint*$(VERSION)*.whl

AUTO_CLEAN := 1

THIRDPARTY_DIR := $(SDIST_DIR)/bots/install


.PHONY: all
all: distinfos bdist_wheel


.PHONY: test pytest
test pytest:
	$(PYTHON) $(SETUP_PY) $@


# LOCALE MESSAGES
.PHONY: locales locale_fr locale_nl
locales: locale_fr locale_nl

locale_fr locale_nl: locale_%:
	@(cd bots && django-admin makemessages -l $* && django-admin compilemessages -l $*)
